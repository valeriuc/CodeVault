#include <stdio.h> // printf
#include <stdlib.h> // EXIT_SUCCESS

#include "prng_shared.h"

int main(int argc, char* argv[]) {
    int seed;
    int N;
    int i;
    double *seq;


    // get parameters from command line arguments
    read_arguments(&seed, &N, argc, argv);

    // allocate and array to hold N random numbers
    seq = malloc(N * sizeof(double));
    // exit if allocation fails
    if (seq == NULL) {
        printf("Cannot allocate space for %d numbers\n", N);
        exit(EXIT_FAILURE);
    }

    // generate N random numbers and save them in the array
    for (i=0; i<N; i++) {
        seq[i] = (double)rand_r(&seed) / (double)RAND_MAX;
    }

    // print the generated random numbers, showing 8 digits after the decimal point
    for (i=0; i<N; i++) {
        printf("%10.8lf\n", seq[i]);
    }

    return EXIT_SUCCESS;

}

