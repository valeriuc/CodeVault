#include <math.h>
#include <stdlib.h>

#include "random.h"

void set_random_seed(int seed)
{
   srand(seed);
}

void array_set_random(double* array, double min, double max, int count)
{
   int i;
   double scale = max - min;

   if(scale > 0.) {
      scale /= (double)RAND_MAX;

      for(i = 0; i < count; i++) {
         array[i] = min + (double)random() * scale;
      }
   } else {
      for(i = 0; i < count; i++) array[i] = min;
   }
}

void vec_set_random(vec_t *vector, double max_magnitude, int count)
{
   double magnitude, angle;
   double mag_scale, ang_scale;
   int i;

   if(max_magnitude > 0.) {
      mag_scale = max_magnitude / (double)RAND_MAX;
      ang_scale = 2 * M_PI / (double)RAND_MAX;

      for(i = 0; i < count; i++) {
         magnitude = mag_scale * (double)random();
         angle = ang_scale * (double)random();

         vector->x[i] = magnitude * cos(angle);
         vector->y[i] = magnitude * sin(angle);
      }
   } else {
      vec_set_zero(vector, count);
   }
}

void vec_set_random_box(vec_t *vector, double min_x, double max_x, double min_y, double max_y, int count)
{
   array_set_random(vector->x, min_x, max_x, count);
   array_set_random(vector->y, min_y, max_y, count);
}


