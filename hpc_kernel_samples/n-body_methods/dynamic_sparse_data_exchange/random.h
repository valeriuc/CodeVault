#ifndef RANDOM_H
#define RANDOM_H

#include "vector.h"

void set_random_seed(int seed);

void array_set_random(double* array, double min, double max, int count);

void vec_set_random(vec_t *vector, double max_magnitude, int count);

void vec_set_random_box(vec_t *vector, double min_x, double max_x, double min_y, double max_y, int count);

#endif

