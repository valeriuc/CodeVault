#pragma once
#include <stdexcept>
#include <utility>
#include <mpi.h>


namespace my
{

struct MPI
{
  MPI(int argc, char * argv[])
  {
    MPI_Init(&argc, &argv);
  }
  ~MPI()
  {
    MPI_Finalize();
  }

  enum Op { MIN, MAX, SUM, PROD };

  static MPI_Op decode_op(Op op)
  {
    switch(op)
    {
      case MIN:  return MPI_MIN;
      case MAX:  return MPI_MAX;
      case SUM:  return MPI_SUM;
      case PROD: return MPI_PROD;
      default:
         throw std::runtime_error("Unkown operation");
    }
  }


  struct Comm
  {
    private:
      MPI_Comm comm_;
      int size_, rank_;

    public:
      Comm(MPI_Comm comm) : comm_(std::move(comm))
    {
      MPI_Comm_size(comm, &size_);
      MPI_Comm_rank(comm, &rank_);
    }

    const MPI_Comm& comm() const { return comm_; }
    int rank() const { return rank_; }
    int size() const { return size_; }
    int root() const { return 0;     }
    bool is_root() const { return rank() == root(); }

    void barrier() const
    {
      MPI_Barrier(comm_);
    }

    template<typename T> T reduce(T value, Op op, int root = 0) const;
    template<typename T> T allreduce(T value, Op op) const;
    template<typename T> void allgather(T value, T* data) const;

  };
};
} // namespace my
