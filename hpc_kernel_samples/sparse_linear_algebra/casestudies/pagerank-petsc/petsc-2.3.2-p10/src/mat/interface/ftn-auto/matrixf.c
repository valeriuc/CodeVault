#include "petsc.h"
#include "petscfix.h"
/* matrix.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscmat.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matrealpart_ MATREALPART
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matrealpart_ matrealpart
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matimaginarypart_ MATIMAGINARYPART
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matimaginarypart_ matimaginarypart
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matconjugate_ MATCONJUGATE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matconjugate_ matconjugate
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetup_ MATSETUP
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetup_ matsetup
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matdestroy_ MATDESTROY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matdestroy_ matdestroy
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matvalid_ MATVALID
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matvalid_ matvalid
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvalues_ MATSETVALUES
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvalues_ matsetvalues
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvaluesrowlocal_ MATSETVALUESROWLOCAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvaluesrowlocal_ matsetvaluesrowlocal
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvaluesrow_ MATSETVALUESROW
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvaluesrow_ matsetvaluesrow
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvaluesstencil_ MATSETVALUESSTENCIL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvaluesstencil_ matsetvaluesstencil
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetstencil_ MATSETSTENCIL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetstencil_ matsetstencil
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvaluesblocked_ MATSETVALUESBLOCKED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvaluesblocked_ matsetvaluesblocked
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetvalues_ MATGETVALUES
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetvalues_ matgetvalues
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetlocaltoglobalmapping_ MATSETLOCALTOGLOBALMAPPING
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetlocaltoglobalmapping_ matsetlocaltoglobalmapping
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetlocaltoglobalmappingblock_ MATSETLOCALTOGLOBALMAPPINGBLOCK
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetlocaltoglobalmappingblock_ matsetlocaltoglobalmappingblock
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvalueslocal_ MATSETVALUESLOCAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvalueslocal_ matsetvalueslocal
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvaluesblockedlocal_ MATSETVALUESBLOCKEDLOCAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvaluesblockedlocal_ matsetvaluesblockedlocal
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmult_ MATMULT
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmult_ matmult
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmulttranspose_ MATMULTTRANSPOSE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmulttranspose_ matmulttranspose
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmultadd_ MATMULTADD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmultadd_ matmultadd
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmulttransposeadd_ MATMULTTRANSPOSEADD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmulttransposeadd_ matmulttransposeadd
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmultconstrained_ MATMULTCONSTRAINED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmultconstrained_ matmultconstrained
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmulttransposeconstrained_ MATMULTTRANSPOSECONSTRAINED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmulttransposeconstrained_ matmulttransposeconstrained
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetinfo_ MATGETINFO
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetinfo_ matgetinfo
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matlufactor_ MATLUFACTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matlufactor_ matlufactor
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matilufactor_ MATILUFACTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matilufactor_ matilufactor
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matlufactorsymbolic_ MATLUFACTORSYMBOLIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matlufactorsymbolic_ matlufactorsymbolic
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matlufactornumeric_ MATLUFACTORNUMERIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matlufactornumeric_ matlufactornumeric
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcholeskyfactor_ MATCHOLESKYFACTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcholeskyfactor_ matcholeskyfactor
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcholeskyfactorsymbolic_ MATCHOLESKYFACTORSYMBOLIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcholeskyfactorsymbolic_ matcholeskyfactorsymbolic
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcholeskyfactornumeric_ MATCHOLESKYFACTORNUMERIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcholeskyfactornumeric_ matcholeskyfactornumeric
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsolve_ MATSOLVE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsolve_ matsolve
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmatsolve_ MATMATSOLVE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmatsolve_ matmatsolve
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsolveadd_ MATSOLVEADD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsolveadd_ matsolveadd
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsolvetranspose_ MATSOLVETRANSPOSE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsolvetranspose_ matsolvetranspose
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsolvetransposeadd_ MATSOLVETRANSPOSEADD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsolvetransposeadd_ matsolvetransposeadd
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matrelax_ MATRELAX
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matrelax_ matrelax
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matpbrelax_ MATPBRELAX
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matpbrelax_ matpbrelax
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcopy_ MATCOPY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcopy_ matcopy
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matduplicate_ MATDUPLICATE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matduplicate_ matduplicate
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetdiagonal_ MATGETDIAGONAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetdiagonal_ matgetdiagonal
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetrowmax_ MATGETROWMAX
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetrowmax_ matgetrowmax
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matequal_ MATEQUAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matequal_ matequal
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matdiagonalscale_ MATDIAGONALSCALE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matdiagonalscale_ matdiagonalscale
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matscale_ MATSCALE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matscale_ matscale
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matnorm_ MATNORM
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matnorm_ matnorm
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matassemblybegin_ MATASSEMBLYBEGIN
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matassemblybegin_ matassemblybegin
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matassembled_ MATASSEMBLED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matassembled_ matassembled
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matassemblyend_ MATASSEMBLYEND
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matassemblyend_ matassemblyend
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcompress_ MATCOMPRESS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcompress_ matcompress
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetoption_ MATSETOPTION
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetoption_ matsetoption
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matzeroentries_ MATZEROENTRIES
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matzeroentries_ matzeroentries
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetsize_ MATGETSIZE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetsize_ matgetsize
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetlocalsize_ MATGETLOCALSIZE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetlocalsize_ matgetlocalsize
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetownershiprange_ MATGETOWNERSHIPRANGE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetownershiprange_ matgetownershiprange
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matilufactorsymbolic_ MATILUFACTORSYMBOLIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matilufactorsymbolic_ matilufactorsymbolic
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define maticcfactorsymbolic_ MATICCFACTORSYMBOLIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define maticcfactorsymbolic_ maticcfactorsymbolic
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matincreaseoverlap_ MATINCREASEOVERLAP
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matincreaseoverlap_ matincreaseoverlap
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetblocksize_ MATGETBLOCKSIZE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetblocksize_ matgetblocksize
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetblocksize_ MATSETBLOCKSIZE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetblocksize_ matsetblocksize
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetunfactored_ MATSETUNFACTORED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetunfactored_ matsetunfactored
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetsubmatrix_ MATGETSUBMATRIX
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetsubmatrix_ matgetsubmatrix
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetsubmatrixraw_ MATGETSUBMATRIXRAW
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetsubmatrixraw_ matgetsubmatrixraw
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matstashsetinitialsize_ MATSTASHSETINITIALSIZE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matstashsetinitialsize_ matstashsetinitialsize
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matinterpolateadd_ MATINTERPOLATEADD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matinterpolateadd_ matinterpolateadd
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matinterpolate_ MATINTERPOLATE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matinterpolate_ matinterpolate
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matrestrict_ MATRESTRICT
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matrestrict_ matrestrict
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define maticcfactor_ MATICCFACTOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define maticcfactor_ maticcfactor
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvaluesadic_ MATSETVALUESADIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvaluesadic_ matsetvaluesadic
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetcoloring_ MATSETCOLORING
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetcoloring_ matsetcoloring
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsetvaluesadifor_ MATSETVALUESADIFOR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsetvaluesadifor_ matsetvaluesadifor
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matdiagonalscalelocal_ MATDIAGONALSCALELOCAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matdiagonalscalelocal_ matdiagonalscalelocal
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetinertia_ MATGETINERTIA
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetinertia_ matgetinertia
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsolves_ MATSOLVES
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsolves_ matsolves
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matissymmetric_ MATISSYMMETRIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matissymmetric_ matissymmetric
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matissymmetricknown_ MATISSYMMETRICKNOWN
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matissymmetricknown_ matissymmetricknown
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matishermitianknown_ MATISHERMITIANKNOWN
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matishermitianknown_ matishermitianknown
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matisstructurallysymmetric_ MATISSTRUCTURALLYSYMMETRIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matisstructurallysymmetric_ matisstructurallysymmetric
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matishermitian_ MATISHERMITIAN
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matishermitian_ matishermitian
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matstashgetinfo_ MATSTASHGETINFO
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matstashgetinfo_ matstashgetinfo
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matgetvecs_ MATGETVECS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matgetvecs_ matgetvecs
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matfactorinfoinitialize_ MATFACTORINFOINITIALIZE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matfactorinfoinitialize_ matfactorinfoinitialize
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matptap_ MATPTAP
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matptap_ matptap
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matptapnumeric_ MATPTAPNUMERIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matptapnumeric_ matptapnumeric
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matptapsymbolic_ MATPTAPSYMBOLIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matptapsymbolic_ matptapsymbolic
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmatmult_ MATMATMULT
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmatmult_ matmatmult
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmatmultsymbolic_ MATMATMULTSYMBOLIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmatmultsymbolic_ matmatmultsymbolic
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmatmultnumeric_ MATMATMULTNUMERIC
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmatmultnumeric_ matmatmultnumeric
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matmatmulttranspose_ MATMATMULTTRANSPOSE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matmatmulttranspose_ matmatmulttranspose
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif

void PETSC_STDCALL   matrealpart_(Mat mat, int *__ierr ){
*__ierr = MatRealPart(
	(Mat)PetscToPointer((mat) ));
}

void PETSC_STDCALL   matimaginarypart_(Mat mat, int *__ierr ){
*__ierr = MatImaginaryPart(
	(Mat)PetscToPointer((mat) ));
}
void PETSC_STDCALL   matconjugate_(Mat mat, int *__ierr ){
*__ierr = MatConjugate(
	(Mat)PetscToPointer((mat) ));
}
void PETSC_STDCALL   matsetup_(Mat A, int *__ierr ){
*__ierr = MatSetUp(
	(Mat)PetscToPointer((A) ));
}
void PETSC_STDCALL   matdestroy_(Mat A, int *__ierr ){
*__ierr = MatDestroy(
	(Mat)PetscToPointer((A) ));
}
void PETSC_STDCALL   matvalid_(Mat m,PetscTruth *flg, int *__ierr ){
*__ierr = MatValid(
	(Mat)PetscToPointer((m) ),flg);
}
void PETSC_STDCALL   matsetvalues_(Mat mat,PetscInt *m, PetscInt idxm[],PetscInt *n, PetscInt idxn[], PetscScalar v[],InsertMode *addv, int *__ierr ){
*__ierr = MatSetValues(
	(Mat)PetscToPointer((mat) ),*m,idxm,*n,idxn,v,*addv);
}
void PETSC_STDCALL   matsetvaluesrowlocal_(Mat mat,PetscInt *row, PetscScalar v[], int *__ierr ){
*__ierr = MatSetValuesRowLocal(
	(Mat)PetscToPointer((mat) ),*row,v);
}
void PETSC_STDCALL   matsetvaluesrow_(Mat mat,PetscInt *row, PetscScalar v[], int *__ierr ){
*__ierr = MatSetValuesRow(
	(Mat)PetscToPointer((mat) ),*row,v);
}
void PETSC_STDCALL   matsetvaluesstencil_(Mat mat,PetscInt *m, MatStencil idxm[],PetscInt *n, MatStencil idxn[], PetscScalar v[],InsertMode *addv, int *__ierr ){
*__ierr = MatSetValuesStencil(
	(Mat)PetscToPointer((mat) ),*m,idxm,*n,idxn,v,*addv);
}
void PETSC_STDCALL   matsetstencil_(Mat mat,PetscInt *dim, PetscInt dims[], PetscInt starts[],PetscInt *dof, int *__ierr ){
*__ierr = MatSetStencil(
	(Mat)PetscToPointer((mat) ),*dim,dims,starts,*dof);
}
void PETSC_STDCALL   matsetvaluesblocked_(Mat mat,PetscInt *m, PetscInt idxm[],PetscInt *n, PetscInt idxn[], PetscScalar v[],InsertMode *addv, int *__ierr ){
*__ierr = MatSetValuesBlocked(
	(Mat)PetscToPointer((mat) ),*m,idxm,*n,idxn,v,*addv);
}
void PETSC_STDCALL   matgetvalues_(Mat mat,PetscInt *m, PetscInt idxm[],PetscInt *n, PetscInt idxn[],PetscScalar v[], int *__ierr ){
*__ierr = MatGetValues(
	(Mat)PetscToPointer((mat) ),*m,idxm,*n,idxn,v);
}
void PETSC_STDCALL   matsetlocaltoglobalmapping_(Mat x,ISLocalToGlobalMapping mapping, int *__ierr ){
*__ierr = MatSetLocalToGlobalMapping(
	(Mat)PetscToPointer((x) ),
	(ISLocalToGlobalMapping)PetscToPointer((mapping) ));
}
void PETSC_STDCALL   matsetlocaltoglobalmappingblock_(Mat x,ISLocalToGlobalMapping mapping, int *__ierr ){
*__ierr = MatSetLocalToGlobalMappingBlock(
	(Mat)PetscToPointer((x) ),
	(ISLocalToGlobalMapping)PetscToPointer((mapping) ));
}
void PETSC_STDCALL   matsetvalueslocal_(Mat mat,PetscInt *nrow, PetscInt irow[],PetscInt *ncol, PetscInt icol[], PetscScalar y[],InsertMode *addv, int *__ierr ){
*__ierr = MatSetValuesLocal(
	(Mat)PetscToPointer((mat) ),*nrow,irow,*ncol,icol,y,*addv);
}
void PETSC_STDCALL   matsetvaluesblockedlocal_(Mat mat,PetscInt *nrow, PetscInt irow[],PetscInt *ncol, PetscInt icol[], PetscScalar y[],InsertMode *addv, int *__ierr ){
*__ierr = MatSetValuesBlockedLocal(
	(Mat)PetscToPointer((mat) ),*nrow,irow,*ncol,icol,y,*addv);
}
void PETSC_STDCALL   matmult_(Mat mat,Vec x,Vec y, int *__ierr ){
*__ierr = MatMult(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   matmulttranspose_(Mat mat,Vec x,Vec y, int *__ierr ){
*__ierr = MatMultTranspose(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   matmultadd_(Mat mat,Vec v1,Vec v2,Vec v3, int *__ierr ){
*__ierr = MatMultAdd(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((v1) ),
	(Vec)PetscToPointer((v2) ),
	(Vec)PetscToPointer((v3) ));
}
void PETSC_STDCALL   matmulttransposeadd_(Mat mat,Vec v1,Vec v2,Vec v3, int *__ierr ){
*__ierr = MatMultTransposeAdd(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((v1) ),
	(Vec)PetscToPointer((v2) ),
	(Vec)PetscToPointer((v3) ));
}
void PETSC_STDCALL   matmultconstrained_(Mat mat,Vec x,Vec y, int *__ierr ){
*__ierr = MatMultConstrained(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   matmulttransposeconstrained_(Mat mat,Vec x,Vec y, int *__ierr ){
*__ierr = MatMultTransposeConstrained(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   matgetinfo_(Mat mat,MatInfoType *flag,MatInfo *info, int *__ierr ){
*__ierr = MatGetInfo(
	(Mat)PetscToPointer((mat) ),*flag,info);
}
void PETSC_STDCALL   matlufactor_(Mat mat,IS row,IS col,MatFactorInfo *info, int *__ierr ){
*__ierr = MatLUFactor(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((row) ),
	(IS)PetscToPointer((col) ),info);
}
void PETSC_STDCALL   matilufactor_(Mat mat,IS row,IS col,MatFactorInfo *info, int *__ierr ){
*__ierr = MatILUFactor(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((row) ),
	(IS)PetscToPointer((col) ),info);
}
void PETSC_STDCALL   matlufactorsymbolic_(Mat mat,IS row,IS col,MatFactorInfo *info,Mat *fact, int *__ierr ){
*__ierr = MatLUFactorSymbolic(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((row) ),
	(IS)PetscToPointer((col) ),info,fact);
}
void PETSC_STDCALL   matlufactornumeric_(Mat mat,MatFactorInfo *info,Mat *fact, int *__ierr ){
*__ierr = MatLUFactorNumeric(
	(Mat)PetscToPointer((mat) ),info,fact);
}
void PETSC_STDCALL   matcholeskyfactor_(Mat mat,IS perm,MatFactorInfo *info, int *__ierr ){
*__ierr = MatCholeskyFactor(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((perm) ),info);
}
void PETSC_STDCALL   matcholeskyfactorsymbolic_(Mat mat,IS perm,MatFactorInfo *info,Mat *fact, int *__ierr ){
*__ierr = MatCholeskyFactorSymbolic(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((perm) ),info,fact);
}
void PETSC_STDCALL   matcholeskyfactornumeric_(Mat mat,MatFactorInfo *info,Mat *fact, int *__ierr ){
*__ierr = MatCholeskyFactorNumeric(
	(Mat)PetscToPointer((mat) ),info,fact);
}
void PETSC_STDCALL   matsolve_(Mat mat,Vec b,Vec x, int *__ierr ){
*__ierr = MatSolve(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((b) ),
	(Vec)PetscToPointer((x) ));
}
void PETSC_STDCALL   matmatsolve_(Mat A,Mat B,Mat X, int *__ierr ){
*__ierr = MatMatSolve(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((B) ),
	(Mat)PetscToPointer((X) ));
}
void PETSC_STDCALL   matsolveadd_(Mat mat,Vec b,Vec y,Vec x, int *__ierr ){
*__ierr = MatSolveAdd(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((b) ),
	(Vec)PetscToPointer((y) ),
	(Vec)PetscToPointer((x) ));
}
void PETSC_STDCALL   matsolvetranspose_(Mat mat,Vec b,Vec x, int *__ierr ){
*__ierr = MatSolveTranspose(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((b) ),
	(Vec)PetscToPointer((x) ));
}
void PETSC_STDCALL   matsolvetransposeadd_(Mat mat,Vec b,Vec y,Vec x, int *__ierr ){
*__ierr = MatSolveTransposeAdd(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((b) ),
	(Vec)PetscToPointer((y) ),
	(Vec)PetscToPointer((x) ));
}
void PETSC_STDCALL   matrelax_(Mat mat,Vec b,PetscReal *omega,MatSORType *flag,PetscReal *shift,PetscInt *its,PetscInt *lits,Vec x, int *__ierr ){
*__ierr = MatRelax(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((b) ),*omega,*flag,*shift,*its,*lits,
	(Vec)PetscToPointer((x) ));
}
void PETSC_STDCALL   matpbrelax_(Mat mat,Vec b,PetscReal *omega,MatSORType *flag,PetscReal *shift,PetscInt *its,PetscInt *lits,Vec x, int *__ierr ){
*__ierr = MatPBRelax(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((b) ),*omega,*flag,*shift,*its,*lits,
	(Vec)PetscToPointer((x) ));
}
void PETSC_STDCALL   matcopy_(Mat A,Mat B,MatStructure *str, int *__ierr ){
*__ierr = MatCopy(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((B) ),*str);
}
void PETSC_STDCALL   matduplicate_(Mat mat,MatDuplicateOption *op,Mat *M, int *__ierr ){
*__ierr = MatDuplicate(
	(Mat)PetscToPointer((mat) ),*op,M);
}
void PETSC_STDCALL   matgetdiagonal_(Mat mat,Vec v, int *__ierr ){
*__ierr = MatGetDiagonal(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((v) ));
}
void PETSC_STDCALL   matgetrowmax_(Mat mat,Vec v, int *__ierr ){
*__ierr = MatGetRowMax(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((v) ));
}
void PETSC_STDCALL   matequal_(Mat A,Mat B,PetscTruth *flg, int *__ierr ){
*__ierr = MatEqual(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((B) ),flg);
}
void PETSC_STDCALL   matdiagonalscale_(Mat mat,Vec l,Vec r, int *__ierr ){
*__ierr = MatDiagonalScale(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((l) ),
	(Vec)PetscToPointer((r) ));
}
void PETSC_STDCALL   matscale_(Mat mat,PetscScalar *a, int *__ierr ){
*__ierr = MatScale(
	(Mat)PetscToPointer((mat) ),*a);
}
void PETSC_STDCALL   matnorm_(Mat mat,NormType *type,PetscReal *nrm, int *__ierr ){
*__ierr = MatNorm(
	(Mat)PetscToPointer((mat) ),*type,nrm);
}
void PETSC_STDCALL   matassemblybegin_(Mat mat,MatAssemblyType *type, int *__ierr ){
*__ierr = MatAssemblyBegin(
	(Mat)PetscToPointer((mat) ),*type);
}
void PETSC_STDCALL   matassembled_(Mat mat,PetscTruth *assembled, int *__ierr ){
*__ierr = MatAssembled(
	(Mat)PetscToPointer((mat) ),assembled);
}
void PETSC_STDCALL   matassemblyend_(Mat mat,MatAssemblyType *type, int *__ierr ){
*__ierr = MatAssemblyEnd(
	(Mat)PetscToPointer((mat) ),*type);
}
void PETSC_STDCALL   matcompress_(Mat mat, int *__ierr ){
*__ierr = MatCompress(
	(Mat)PetscToPointer((mat) ));
}
void PETSC_STDCALL   matsetoption_(Mat mat,MatOption *op, int *__ierr ){
*__ierr = MatSetOption(
	(Mat)PetscToPointer((mat) ),*op);
}
void PETSC_STDCALL   matzeroentries_(Mat mat, int *__ierr ){
*__ierr = MatZeroEntries(
	(Mat)PetscToPointer((mat) ));
}
void PETSC_STDCALL   matgetsize_(Mat mat,PetscInt *m,PetscInt* n, int *__ierr ){
*__ierr = MatGetSize(
	(Mat)PetscToPointer((mat) ),m,n);
}
void PETSC_STDCALL   matgetlocalsize_(Mat mat,PetscInt *m,PetscInt* n, int *__ierr ){
*__ierr = MatGetLocalSize(
	(Mat)PetscToPointer((mat) ),m,n);
}
void PETSC_STDCALL   matgetownershiprange_(Mat mat,PetscInt *m,PetscInt* n, int *__ierr ){
*__ierr = MatGetOwnershipRange(
	(Mat)PetscToPointer((mat) ),m,n);
}
void PETSC_STDCALL   matilufactorsymbolic_(Mat mat,IS row,IS col,MatFactorInfo *info,Mat *fact, int *__ierr ){
*__ierr = MatILUFactorSymbolic(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((row) ),
	(IS)PetscToPointer((col) ),info,fact);
}
void PETSC_STDCALL   maticcfactorsymbolic_(Mat mat,IS perm,MatFactorInfo *info,Mat *fact, int *__ierr ){
*__ierr = MatICCFactorSymbolic(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((perm) ),info,fact);
}
void PETSC_STDCALL   matincreaseoverlap_(Mat mat,PetscInt *n,IS is[],PetscInt *ov, int *__ierr ){
*__ierr = MatIncreaseOverlap(
	(Mat)PetscToPointer((mat) ),*n,is,*ov);
}
void PETSC_STDCALL   matgetblocksize_(Mat mat,PetscInt *bs, int *__ierr ){
*__ierr = MatGetBlockSize(
	(Mat)PetscToPointer((mat) ),bs);
}
void PETSC_STDCALL   matsetblocksize_(Mat mat,PetscInt *bs, int *__ierr ){
*__ierr = MatSetBlockSize(
	(Mat)PetscToPointer((mat) ),*bs);
}
void PETSC_STDCALL   matsetunfactored_(Mat mat, int *__ierr ){
*__ierr = MatSetUnfactored(
	(Mat)PetscToPointer((mat) ));
}
void PETSC_STDCALL   matgetsubmatrix_(Mat mat,IS isrow,IS iscol,PetscInt *csize,MatReuse *cll,Mat *newmat, int *__ierr ){
*__ierr = MatGetSubMatrix(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((isrow) ),
	(IS)PetscToPointer((iscol) ),*csize,*cll,newmat);
}
void PETSC_STDCALL   matgetsubmatrixraw_(Mat mat,PetscInt *nrows, PetscInt rows[],PetscInt *ncols, PetscInt cols[],PetscInt *csize,MatReuse *cll,Mat *newmat, int *__ierr ){
*__ierr = MatGetSubMatrixRaw(
	(Mat)PetscToPointer((mat) ),*nrows,rows,*ncols,cols,*csize,*cll,newmat);
}
void PETSC_STDCALL   matstashsetinitialsize_(Mat mat,PetscInt *size,PetscInt *bsize, int *__ierr ){
*__ierr = MatStashSetInitialSize(
	(Mat)PetscToPointer((mat) ),*size,*bsize);
}
void PETSC_STDCALL   matinterpolateadd_(Mat A,Vec x,Vec y,Vec w, int *__ierr ){
*__ierr = MatInterpolateAdd(
	(Mat)PetscToPointer((A) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ),
	(Vec)PetscToPointer((w) ));
}
void PETSC_STDCALL   matinterpolate_(Mat A,Vec x,Vec y, int *__ierr ){
*__ierr = MatInterpolate(
	(Mat)PetscToPointer((A) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   matrestrict_(Mat A,Vec x,Vec y, int *__ierr ){
*__ierr = MatRestrict(
	(Mat)PetscToPointer((A) ),
	(Vec)PetscToPointer((x) ),
	(Vec)PetscToPointer((y) ));
}
void PETSC_STDCALL   maticcfactor_(Mat mat,IS row,MatFactorInfo* info, int *__ierr ){
*__ierr = MatICCFactor(
	(Mat)PetscToPointer((mat) ),
	(IS)PetscToPointer((row) ),info);
}
void PETSC_STDCALL   matsetvaluesadic_(Mat mat,void*v, int *__ierr ){
*__ierr = MatSetValuesAdic(
	(Mat)PetscToPointer((mat) ),v);
}
void PETSC_STDCALL   matsetcoloring_(Mat mat,ISColoring coloring, int *__ierr ){
*__ierr = MatSetColoring(
	(Mat)PetscToPointer((mat) ),
	(ISColoring)PetscToPointer((coloring) ));
}
void PETSC_STDCALL   matsetvaluesadifor_(Mat mat,PetscInt *nl,void*v, int *__ierr ){
*__ierr = MatSetValuesAdifor(
	(Mat)PetscToPointer((mat) ),*nl,v);
}
void PETSC_STDCALL   matdiagonalscalelocal_(Mat mat,Vec diag, int *__ierr ){
*__ierr = MatDiagonalScaleLocal(
	(Mat)PetscToPointer((mat) ),
	(Vec)PetscToPointer((diag) ));
}
void PETSC_STDCALL   matgetinertia_(Mat mat,PetscInt *nneg,PetscInt *nzero,PetscInt *npos, int *__ierr ){
*__ierr = MatGetInertia(
	(Mat)PetscToPointer((mat) ),nneg,nzero,npos);
}
void PETSC_STDCALL   matsolves_(Mat mat,Vecs b,Vecs x, int *__ierr ){
*__ierr = MatSolves(
	(Mat)PetscToPointer((mat) ),
	(Vecs)PetscToPointer((b) ),
	(Vecs)PetscToPointer((x) ));
}
void PETSC_STDCALL   matissymmetric_(Mat A,PetscReal *tol,PetscTruth *flg, int *__ierr ){
*__ierr = MatIsSymmetric(
	(Mat)PetscToPointer((A) ),*tol,flg);
}
void PETSC_STDCALL   matissymmetricknown_(Mat A,PetscTruth *set,PetscTruth *flg, int *__ierr ){
*__ierr = MatIsSymmetricKnown(
	(Mat)PetscToPointer((A) ),set,flg);
}
void PETSC_STDCALL   matishermitianknown_(Mat A,PetscTruth *set,PetscTruth *flg, int *__ierr ){
*__ierr = MatIsHermitianKnown(
	(Mat)PetscToPointer((A) ),set,flg);
}
void PETSC_STDCALL   matisstructurallysymmetric_(Mat A,PetscTruth *flg, int *__ierr ){
*__ierr = MatIsStructurallySymmetric(
	(Mat)PetscToPointer((A) ),flg);
}
void PETSC_STDCALL   matishermitian_(Mat A,PetscTruth *flg, int *__ierr ){
*__ierr = MatIsHermitian(
	(Mat)PetscToPointer((A) ),flg);
}
void PETSC_STDCALL   matstashgetinfo_(Mat mat,PetscInt *nstash,PetscInt *reallocs,PetscInt *bnstash,PetscInt *breallocs, int *__ierr ){
*__ierr = MatStashGetInfo(
	(Mat)PetscToPointer((mat) ),nstash,reallocs,bnstash,breallocs);
}
void PETSC_STDCALL   matgetvecs_(Mat mat,Vec *right,Vec *left, int *__ierr ){
*__ierr = MatGetVecs(
	(Mat)PetscToPointer((mat) ),right,left);
}

void PETSC_STDCALL   matfactorinfoinitialize_(MatFactorInfo *info, int *__ierr ){
*__ierr = MatFactorInfoInitialize(info);
}
void PETSC_STDCALL   matptap_(Mat A,Mat P,MatReuse *scall,PetscReal *fill,Mat *C, int *__ierr ){
*__ierr = MatPtAP(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((P) ),*scall,*fill,C);
}
void PETSC_STDCALL   matptapnumeric_(Mat A,Mat P,Mat C, int *__ierr ){
*__ierr = MatPtAPNumeric(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((P) ),
	(Mat)PetscToPointer((C) ));
}
void PETSC_STDCALL   matptapsymbolic_(Mat A,Mat P,PetscReal *fill,Mat *C, int *__ierr ){
*__ierr = MatPtAPSymbolic(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((P) ),*fill,C);
}
void PETSC_STDCALL   matmatmult_(Mat A,Mat B,MatReuse *scall,PetscReal *fill,Mat *C, int *__ierr ){
*__ierr = MatMatMult(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((B) ),*scall,*fill,C);
}
void PETSC_STDCALL   matmatmultsymbolic_(Mat A,Mat B,PetscReal *fill,Mat *C, int *__ierr ){
*__ierr = MatMatMultSymbolic(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((B) ),*fill,C);
}
void PETSC_STDCALL   matmatmultnumeric_(Mat A,Mat B,Mat C, int *__ierr ){
*__ierr = MatMatMultNumeric(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((B) ),
	(Mat)PetscToPointer((C) ));
}
void PETSC_STDCALL   matmatmulttranspose_(Mat A,Mat B,MatReuse *scall,PetscReal *fill,Mat *C, int *__ierr ){
*__ierr = MatMatMultTranspose(
	(Mat)PetscToPointer((A) ),
	(Mat)PetscToPointer((B) ),*scall,*fill,C);
}
#if defined(__cplusplus)
}
#endif
