#include "petsc.h"
#include "petscfix.h"
/* dupl.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscviewer.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define petscviewergetsingleton_ PETSCVIEWERGETSINGLETON
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define petscviewergetsingleton_ petscviewergetsingleton
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define petscviewerrestoresingleton_ PETSCVIEWERRESTORESINGLETON
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define petscviewerrestoresingleton_ petscviewerrestoresingleton
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   petscviewergetsingleton_(PetscViewer viewer,PetscViewer *outviewer, int *__ierr ){
*__ierr = PetscViewerGetSingleton(
	(PetscViewer)PetscToPointer((viewer) ),outviewer);
}
void PETSC_STDCALL   petscviewerrestoresingleton_(PetscViewer viewer,PetscViewer *outviewer, int *__ierr ){
*__ierr = PetscViewerRestoreSingleton(
	(PetscViewer)PetscToPointer((viewer) ),outviewer);
}
#if defined(__cplusplus)
}
#endif
