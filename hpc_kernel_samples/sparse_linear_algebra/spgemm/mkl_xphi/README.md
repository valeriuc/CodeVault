=======
README
=======
- 1. Code sample name
mklspgemm

- 2. Description of the code sample package
mklspgemm code performs multiplication of two sparse matrices using MKL's routine mkl_dcsrmultcsr() on a Xeon Phi coprocessor.
Offload mode is used.

MKL library for Xeon Phi is required. Note that MKL library for Xeon Phi is in different from the one for host system.

This is one of the code sample from the PRACE CodeVault. You can find more code samples available for download from the PRACE CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault 

- 3. Release date
26 January 2016

- 4. Version history 
1.0: initial version

- 5. Contributor (s) / Maintainer(s) 
Kadir Akbudak <kadir.cs@gmail.com>

- 6. Copyright / License of the code sample
Apache 2.0

- 7. Language(s) 
C

- 8. Parallelisation Implementation(s)
Parallelism is provided by MKL.

- 9. Level of the code sample complexity 
new starts

- 10. Instructions on how to compile the code
cc=icc cmake .
make

- 11. Instructions on how to run the code
export OFFLOAD_INIT=on_offload;./mklspgemm test.mtx test.mtx out.mtx 2 0 PRINT_YES
1. ./mklspgemm : Executable
2. test.mtx  : First input matrix
3. test.mtx  : Second input matrix
4. out.mtx   : Output matrix
5. 2         : Number of threads
6. 0         : Id of the mic device. It must be a single integer.
7. PRINT_YES : Print matrices to stdout

export OFFLOAD_INIT=on_offload; starts only on of the mic cards when there are more than one mic card. If you avoid this environment variable, systems tries to start all mic cards so runs on other cards are effected.

- 12. Sample input(s)
test.mtx contains a small square matrix in Matrix Market format [1]

- 13. Sample output(s)
Prints timing information to output.

REFERENCES:
[1] http://math.nist.gov/MatrixMarket/formats.html
