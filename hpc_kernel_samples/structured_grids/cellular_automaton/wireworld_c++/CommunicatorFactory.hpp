#pragma once

#include <array>
#include <memory>
#include <tuple>

#include "CollectiveCommunicator.hpp"
#include "CommunicationMode.hpp"
#include "Configuration.hpp"
#include "P2PCommunicator.hpp"
#include "Tile.hpp"
#include "Util.hpp"

struct CommunicatorFactory {
	static std::unique_ptr<Communicator> Create(Configuration cfg, const Tile& tile, const MpiEnvironment& env) {
		switch (cfg.CommMode) {
		case CommunicationMode::Collective:
			return std::make_unique<CollectiveCommunicator>(env, cfg.Procs, tile.tileSize());
		case CommunicationMode::P2P:
			return std::make_unique<P2PCommunicator>(env, cfg.Procs, tile.tileSize());
		default:
			MpiReportErrorAbort("Unknown CommunicationMode");
		}
	}
};
