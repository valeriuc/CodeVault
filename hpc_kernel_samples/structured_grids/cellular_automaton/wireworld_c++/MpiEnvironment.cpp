#include "MpiEnvironment.hpp"

#include <utility>

void MpiEnvironment::swap(MpiEnvironment& first,
                          MpiEnvironment& second) noexcept {
	using std::swap;
	swap(first.worldRank_, second.worldRank_);
	swap(first.worldSize_, second.worldSize_);
	swap(first.isMaster_, second.isMaster_);
}

MpiEnvironment::MpiEnvironment(int& argc, char* argv[]) {
	MPI_Init(&argc, &argv);
	worldRank_ = [] { int r;  MPI_Comm_rank(MPI_COMM_WORLD, &r); return static_cast<std::size_t>(r); }();
	worldSize_ = [] { int s;  MPI_Comm_size(MPI_COMM_WORLD, &s); return static_cast<std::size_t>(s); }();
	isMaster_ = {worldRank_ == 0};
	// We want the program to stop on I/O errors
	MPI_File_set_errhandler(MPI_FILE_NULL, MPI_ERRORS_ARE_FATAL);
}
MpiEnvironment::~MpiEnvironment() {
	if (worldRank_ != std::numeric_limits<std::size_t>::max()) { MPI_Finalize(); }
}

MpiEnvironment::MpiEnvironment(MpiEnvironment&& other) noexcept {
	swap(*this, other);
}
MpiEnvironment& MpiEnvironment::operator=(MpiEnvironment&& other) noexcept {
	swap(*this, other);
	return *this;
}
