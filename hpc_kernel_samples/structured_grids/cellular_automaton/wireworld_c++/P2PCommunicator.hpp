#pragma once

#include "Communicator.hpp"

struct P2PCommunicator : public Communicator {
	using Communicator::Communicator;

	void Communicate(State* model) override;
	MpiRequest AsyncCommunicate(State* model) override;
};
