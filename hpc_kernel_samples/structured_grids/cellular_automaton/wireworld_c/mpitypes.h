#ifndef MPITYPES_H
#define MPITYPES_H

#include <mpi.h>

void mpitype_conf_init(MPI_Datatype *new_type);
void mpitype_conf_free(MPI_Datatype *type);

#endif

