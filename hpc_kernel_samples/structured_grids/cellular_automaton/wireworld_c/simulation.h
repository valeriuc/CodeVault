#ifndef _AUTOMATON_H_
#define _AUTOMATON_H_

#include "world.h"
#include "configuration.h"

void do_simulation(world_t *world, size_t n_generations, const conf_t *c);

#endif
