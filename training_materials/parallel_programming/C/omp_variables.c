/*
   Copyright (C) 2015  CSC - IT Center for Science Ltd.

   Licensed under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Code is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   Copy of the GNU General Public License can be onbtained from
   see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>

int main(void)
{
    int var1 = 1, var2 = 2;

#pragma omp parallel private(var1, var2)
    {
        printf("Region 1: var1=%i, var2=%i\n", var1, var2);
        var1++;
        var2++;
    }
    printf("After region 1: var1=%i, var2=%i\n\n", var1, var2);

#pragma omp parallel firstprivate(var1, var2)
    {
        printf("Region 2: var1=%i, var2=%i\n", var1, var2);
        var1++;
        var2++;
    }
    printf("After region 2: var1=%i, var2=%i\n\n", var1, var2);

#pragma omp parallel            /* same as omp parallel shared(var1, var2) */
    {
        printf("Region 3: var1=%i, var2=%i\n", var1, var2);
        /* Note that this introduces the data race condition! */
        var1++;
        var2++;
    }
    printf("After region 3: var1=%i, var2=%i\n\n", var1, var2);

    return 0;
}
